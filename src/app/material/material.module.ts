import { NgModule } from '@angular/core';

import { MatSliderModule } from '@angular/material/slider';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTableModule } from '@angular/material/table';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDialogModule } from '@angular/material/dialog';

const modules = [
  MatIconModule,
  MatInputModule,
  MatButtonModule,
  MatSliderModule,
  MatToolbarModule,
  MatSelectModule,
  MatMenuModule,
  MatSidenavModule,
  MatTableModule,
  MatCheckboxModule,
  MatSnackBarModule,
  MatTooltipModule,
  MatDialogModule,
];

@NgModule({
  imports: modules,
  providers: [],
  exports: modules,
})
export class MaterialModule {
}
