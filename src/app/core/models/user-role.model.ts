export interface UserRoleModel {
  id: number;
  name: string;
  displayName: string;
  permissions: string[];
}
