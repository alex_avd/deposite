import { PlantClientModel } from './plant-client.model';

export interface PlantModel {
  id: number;
  name: string;
  organization: PlantClientModel
}
