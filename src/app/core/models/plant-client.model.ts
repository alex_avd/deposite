export interface PlantClientModel {
  id: number;
  username: string;
  email: string;
}
