import { PlantModel } from './plant.model';
import { UserRoleModel } from './user-role.model';

export interface UserDataModel {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  userRole: UserRoleModel;
  availablePlants: PlantModel[];
}
