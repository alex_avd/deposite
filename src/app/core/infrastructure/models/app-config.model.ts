export interface AppConfigModel {
  API: {
    AUTOMATION_STANDARD: string;
    VOICE_RECORDER: string;
  };
  VERSION: string;
}
