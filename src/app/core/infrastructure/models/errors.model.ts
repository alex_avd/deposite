export interface ErrorsModel {
  arguments: any[];
  bindingFailure: boolean;
  code: string;
  codes: string[]; // 0: "NotNull.conditionId" 1: "NotNull.java.lang.String"
  defaultMessage: string;
  field: string;
  objectName: string;
  rejectedValue: any;
}
