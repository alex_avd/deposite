import { ErrorsModel } from './errors.model';

export interface ErrorDataModel {
  errors: ErrorsModel[];
  message: string;
  timestamp: string;
}
