import { HttpStatus } from '@hmx/system-ng';
import { HttpErrorResponse } from '@angular/common/http';

import { ErrorsService } from './errors.service';
import { ErrorCodeEnum } from '../models/error-code.enum';

describe('Errors Service', () => {
  let errorsService: ErrorsService;
  const portalServiceMock = {
    send: jest.fn(),
  } as any;

  const injectorMock = {
    get: jest.fn().mockReturnValue(portalServiceMock),
  } as any;

  beforeEach(() => {
    errorsService = new ErrorsService(injectorMock);
  });

  test('parseServerErrors should catch unauthorized error', () => {
    // Arrange
    const error = { status: HttpStatus.UNAUTHORIZED };
    const httpErrorResponse: HttpErrorResponse = new HttpErrorResponse(error);

    // Act
    const actualError = errorsService.parseServerErrors(httpErrorResponse);

    // Assert
    expect(portalServiceMock.send).toHaveBeenCalledTimes(1);
    expect(actualError).toBeFalsy();
  });

  test('parseServerErrors should catch default server error', () => {
    // Arrange
    const statusText = 'Unknown Error';
    const expectedError = ['Unknown Error. Please try again'];
    const error = {
      status: ErrorCodeEnum.DEFAULT_SERVER_ERROR,
      statusText,
    };
    const httpErrorResponse: HttpErrorResponse = new HttpErrorResponse(error);

    // Act
    const actualError = errorsService.parseServerErrors(httpErrorResponse);

    // Assert
    expect(actualError).toEqual(expectedError);
  });

  test('parseServerErrors should catch forbidden errors', () => {
    // Arrange
    const error = {
      status: HttpStatus.FORBIDDEN,
    };
    const httpErrorResponse: HttpErrorResponse = new HttpErrorResponse(error);
    const expectedError = ['The action that you requested is Forbidden.'];

    // Act
    const actualError = errorsService.parseServerErrors(httpErrorResponse);

    // Assert
    expect(actualError).toEqual(expectedError);
  });

  test('parseServerErrors should catch bad request', () => {
    // Arrange
    const error = {
      status: HttpStatus.BAD_REQUEST,
    };
    const httpErrorResponse: HttpErrorResponse = new HttpErrorResponse(error);
    const expectedError = ['Unable to process the request.'];

    // Act
    const actualError = errorsService.parseServerErrors(httpErrorResponse);

    // Assert
    expect(actualError).toEqual(expectedError);
  });

  test('parseServerErrors should catch server errors', () => {
    // Arrange
    const error = {
      status: 408,
      error: {
        errorData: {
          errors: [
            {
              field: '',
              defaultMessage: 'error1',
            },
            {
              field: 'field1',
              defaultMessage: 'error1',
            },
            {
              field: 'field2',
              defaultMessage: 'error2',
            },
          ],
        },
      },
    };
    const expectedErrors = [
      ' error1',
      'field1 error1',
      'field2 error2',
    ];
    const httpErrorResponse: HttpErrorResponse = new HttpErrorResponse(error);

    // Act
    const actualErrors = errorsService.parseServerErrors(httpErrorResponse);

    // Assert
    expect(actualErrors).toEqual(expectedErrors);
  });
});
