import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { UserTokenInterceptor } from './user-token.interceptor';

describe('Agency Id Interceptor', () => {
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: UserTokenInterceptor,
          multi: true,
        },
      ],
    });
    httpClient = TestBed.inject(HttpClient);
    httpMock = TestBed.inject(HttpTestingController);

    sessionStorage.clear();
  });

  test('should miss in request header, agency id key, for not authenticated user', () => {
    // Arrange
    const url = 'url';

    // Act
    httpClient.get(url).subscribe();
    const httpRequest = httpMock.expectOne(url);
    httpMock.verify();

    // Assert
    expect(httpRequest.request.headers.get('X-Agency-Id')).toBe(null);
  });

  test('should add in request header, agency id key, for authenticated user', () => {
    // Arrange
    const url = 'url';
    const agencyId = 1000;
    sessionStorage.setItem('authUser', JSON.stringify({ agencyId }));

    // Act
    httpClient.get(url).subscribe();
    const httpRequest = httpMock.expectOne(url);
    httpMock.verify();

    // Assert
    expect(httpRequest.request.headers.get('X-Agency-Id')).toBe(agencyId.toString());
  });
});
