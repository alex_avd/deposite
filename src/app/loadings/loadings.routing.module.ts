import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { LoadingsComponent } from './containers/loadings-container/loadings.component';

const routes: Routes = [
  { path: '', component: LoadingsComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class LoadingsRoutingModule {

}
