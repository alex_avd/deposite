import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { LoadingsService } from '../../services/loadings.service';

export interface PeriodicElementModel {
  id: string;
  title: string;
  status: string;
  field: string;
  date: string;
  time: string;
}

const ELEMENT_DATA: PeriodicElementModel[] = [
  { id: '1', title: 'Gem detected', status: 'PENDING', field: 'E', date: '01/01/2020', time: '11:45' },
  { id: '2', title: 'Danger zone activity', status: 'PENDING', field: 'H', date: '08/16/2020', time: '12:35' },
  { id: '3', title: 'Gem detected', status: 'CLOSED', field: 'E', date: '01/01/2020', time: '11:40' },

];

@Component({
  selector: 'app-loading-grid',
  templateUrl: './loading-grid.component.html',
  styleUrls: ['./loading-grid.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LoadingGridComponent implements OnInit {

  displayedColumns: string[] = ['id', 'title', 'status', 'field', 'date', 'time'];
  dataSource = ELEMENT_DATA;
  limit = '10';

  depositAsync$ = this.loadingsService.getDeposit(this.limit);
  categoriesAsync$ = this.loadingsService.getAsyncCategories();
  titlesAsync$ = this.loadingsService.getAsyncTitles(this.categoriesAsync$);
  getArrivalAsync$ = this.loadingsService.getArrival(this.limit);
  getDepartureAsync$ = this.loadingsService.getDeparture(this.limit);

  constructor(private loadingsService: LoadingsService) {
  }

  ngOnInit(): void {
  }

}
