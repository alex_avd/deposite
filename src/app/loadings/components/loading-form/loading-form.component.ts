import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-loading-form',
  templateUrl: './loading-form.component.html',
  styleUrls: ['./loading-form.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LoadingFormComponent implements OnInit {

  cargoForm: FormGroup;

  get positions() {
    return this.cargoForm.get('positions') as FormArray;
  }

  ngOnInit() {
    this.cargoForm = new FormGroup({
      carType: new FormControl('', [Validators.required]),
      driverName: new FormControl('', [Validators.required]),
      positions: new FormArray([
        new FormGroup({
          metal: new FormControl(''),
          weight: new FormControl(''),
        }),
      ]),
    });
  }

  addLine() {
    this.positions.push(new FormGroup({
      metal: new FormControl(''),
      weight: new FormControl(''),
    }));
    console.log(this.cargoForm.value, 111);
  }

  save() {
    console.log(this.cargoForm.value);
  }

}
