import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-reports',
  templateUrl: './loadings.component.html',
  styleUrls: ['./loadings.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LoadingsComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }

}
