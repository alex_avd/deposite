import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class LoadingsService {

  constructor(private http: HttpClient) {
  }

  getshow() {
    return this.http.get('http://deposite.ru/index.php');
  }

  getDeposit(limit: string) {
    const params = new HttpParams()
      .set('action', 'deposit')
      .set('limit', limit);
    return this.http.get('http://deposite.ru/deposit.php', { params });
  }

  getArrival(limit: any) {
    const params = new HttpParams()
      .set('action', 'getarrival')
      .set('limit', limit);
    return this.http.get('http://deposite.ru/index.php', { params });
  }

  getDeparture(limit: string) {
    const params = new HttpParams()
      .set('action', 'getdeparture')
      .set('limit', limit);
    return this.http.get('http://deposite.ru/index.php', { params });
  }

  deleteAsynDeposit(table: string, id: string) {
    const params = new HttpParams()
      .set('table', table)
      .set('id', id);
    return this.http.post('http://deposite.ru/deleteitem.php', params);
  }

  AddProduct(params: any) {
    return this.http.post('http://deposite.ru/saveitem.php', params);
  }


  saveAsynDeparture(params: any) {
    return this.http.post('http://deposite.ru/departure.php', params);
  }

  saveAsynArrival(params: any) {
    return this.http.post('http://deposite.ru/arrival.php', params);
  }

  getAsyncCategories() {
    // console.log('Scucces and load new data')
    const params = new HttpParams()
      .set('action', 'category');
    return this.http.get('http://deposite.ru/index.php', { params });
  }

  getAsyncTitles(selectedValue: any) {
    const params = new HttpParams()
      .set('action', 'titles')
      .set('title', selectedValue);
    return this.http.get('http://deposite.ru/index.php', { params });

  }

  getAsyncTitles2(selectedValue: any) {
    const params = new HttpParams()
      .set('action', 'titles')
      .set('title', selectedValue);
    return this.http.get('http://deposite.ru/index.php', { params });

  }

  getAsyncTitles3(selectedValue: any) {
    const params = new HttpParams()
      .set('action', 'titles')
      .set('title', selectedValue);
    return this.http.get('http://deposite.ru/index.php', { params });

  }

}
