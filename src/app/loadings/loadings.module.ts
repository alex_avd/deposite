import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MaterialModule } from '../material';
import { LoadingsRoutingModule } from './loadings.routing.module';
import { LoadingsComponent } from './containers/loadings-container/loadings.component';
import { LoadingGridComponent } from './components/loading-grid/loading-grid.component';
import { LoadingFormComponent } from './components/loading-form/loading-form.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    LoadingGridComponent,
    LoadingFormComponent,
    LoadingsComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    LoadingsRoutingModule,
    FlexLayoutModule,
  ],
  providers: [],
})
export class LoadingsModule {
}
