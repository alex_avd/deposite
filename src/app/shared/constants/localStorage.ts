export const LOCAL_STORAGE = {
  USER_DATA: 'userData',
  USER_TOKEN: 'userToken',
  PLANT_DATA: 'plantData',
};
