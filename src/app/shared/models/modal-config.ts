export interface ModalConfig {
  modalComponent?: any;
  id?: string;
  headerText?: string;
  bodyText?: string;
  infoIcon?: boolean;
  okButton?: string;
  cancelButton?: string;
}
