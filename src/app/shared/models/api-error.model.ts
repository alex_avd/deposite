import { ApiErrorDetailsModel } from './api-error-details.model';

export interface ApiErrorModel {
  errors: ApiErrorDetailsModel[];
  timestamp: string;
}
