export interface ApiErrorDetailsModel {
  code: string;
  defaultMessage: string;
}
