import { ApiErrorModel } from './api-error.model';

export interface ApiResponseModel<T> {
  data: T;
  errorData?: ApiErrorModel;
}
