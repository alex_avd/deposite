import { DefaultModalComponent } from './default-modal.component';

describe('DefaultModalComponent', () => {
  let component: DefaultModalComponent;
  const dialogRefMock = {
    close: jest.fn(),
  } as any;

  beforeEach(() => {
    component = new DefaultModalComponent(dialogRefMock);
  });

  describe('submitFn', () => {
    test('should close modal when submitted', () => {
      // Act
      component.submitFn();

      // Assert
      expect(dialogRefMock.close).toHaveBeenCalled();
    });
  });
});
