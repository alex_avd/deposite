import { SnackBarComponent } from './snack-bar.component';

describe('SnackBarComponent', () => {
  let component: SnackBarComponent;
  const matSnackBarRef = {
    dismiss: jest.fn(),
  } as any;
  const data = {
    onConfirm: jest.fn(),
  };

  beforeEach(() => {
    component = new SnackBarComponent(data, matSnackBarRef);
  });

  describe('snackBarError', () => {
    test('should set snackBarError to be true if isGlobalError is true', () => {
    // Arrange
      component.data.message = 'Message';
      component.data.isGlobalError = true;

      // Assert
      expect(component.snackBarError()).toBe(true);
    });

    test('should set snackBarError to be false if isGlobalError is false', () => {
    // Act
      component.data.message = 'Message';
      component.data.isGlobalError = false;

      // Assert
      expect(component.snackBarError()).toBe(false);
    });
  });

  describe('onSnackBarConfirm', () => {
    test('should call data.confirm and matRef dismiss onSnackBarConfirm when invoked', () => {
      // Arrange
      component.data = {
        onConfirm: jest.fn(),
      };

      // Act
      component.onSnackBarConfirm();

      // Assert
      expect(matSnackBarRef.dismiss).toHaveBeenCalled();
      expect(component.data.onConfirm).toHaveBeenCalled();
    });
  });

  describe('onSnackBarClose', () => {
    test('should call matRef dismiss onSnackBarClose when invoked', () => {
      // Act
      component.onSnackBarClose();

      // Assert
      expect(matSnackBarRef.dismiss).toHaveBeenCalled();
    });
  });
});
