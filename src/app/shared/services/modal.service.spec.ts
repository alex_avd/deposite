import { MatDialog } from '@angular/material/dialog';
import { Component } from '@angular/core';
import { of } from 'rxjs';

import { ModalService } from './modal.service';
import { ModalConfig } from '../models/modal-config';

@Component({})
class TestComponent {

  config: ModalConfig = {
    modalComponent: undefined,
    okButton: 'OK',
    cancelButton: undefined,
  };

}

describe('ModalService', () => {
  let service: ModalService;
  let dialog: MatDialog;

  beforeEach(() => {
    dialog = new MatDialog(
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
    );
    service = new ModalService(dialog);
  });

  it('should create an instance when initialized', () => {
    expect(service).toBeDefined();
  });

  describe('openModal', () => {
    it('should open dialog and return modalRef when called', () => {
      // Arrange
      jest.spyOn(dialog, 'open').mockReturnValueOnce({ componentInstance: { config: '' } } as any);

      // Act
      service.openModal({ modalComponent: TestComponent });

      // Assert
      expect(dialog.open).toHaveBeenCalled();
    });
  });

  describe('showConfirm', () => {
    it('should open confirmation dialog when invoked', () => {
      // Arrange
      jest.spyOn(service, 'openModal').mockReturnValueOnce({
        afterClosed: () => of(true),
      } as any);

      // Act
      service.showConfirm({ id: 'mock-id' });

      // Assert
      expect(service.openModal).toHaveBeenCalled();
    });

    it('should open confirmation dialog when called with options', () => {
      // Arrange
      jest.spyOn(service, 'openModal').mockReturnValueOnce({
        afterClosed: () => of(true),
      } as any);

      // Act
      service.showConfirm({ id: 'mock-id' }, { height: '178px', width: '584px' });

      // Assert
      expect(service.openModal).toHaveBeenCalled();
    });
  });

  describe('getDialogRef', () => {
    test('should getDialogRef by type', () => {
      // Arrange
      const spy = jest.spyOn(dialog.openDialogs as any, 'find');

      // Act
      service.getDialogRef({});

      // Assert
      expect(spy).toHaveBeenCalled();
    });
  });
});
