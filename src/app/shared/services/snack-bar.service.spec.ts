import { MatSnackBarConfig } from '@angular/material/snack-bar';
import { of } from 'rxjs';

import { SnackbarService } from './snack-bar.service';

describe('SnackBar Service', () => {
  let snackService: SnackbarService;
  const matSnackBar = { openFromComponent: jest.fn() } as any;
  const ngZone = ({ run: jest.fn() }) as any;

  beforeEach(() => {
    snackService = new SnackbarService(matSnackBar, ngZone);
  });

  test('should init the service when initialized', () => {
    expect(snackService).toBeDefined();
  });

  describe('showNext', () => {
    test('should be called recursively 2 times when having a message in msgQueue', () => {
      // Arrange
      (snackService as any).msgQueue = [{ obj: '1' }];
      (snackService as any).snackBarRef = { afterDismissed: () => of('someResult') };

      const spy = jest.spyOn(snackService, 'showNext');

      // Act
      snackService.showNext();

      // Assert
      expect(spy).toHaveBeenCalledTimes(2);
    });

    test('should be called recursively 3 times when having 2 messages in msgQueue', () => {
      // Arrange
      (snackService as any).msgQueue = [{ msg: '1' }, { msg: '2' }];
      (snackService as any).snackBarRef = { afterDismissed: () => of('someResult') };
      const spy = jest.spyOn(snackService, 'showNext');

      // Act
      snackService.showNext();

      // Assert
      expect(spy).toHaveBeenCalledTimes(3);
    });
  });

  describe('openSnackBar', () => {
    test('should not trigger openFromComponent when msgQueue is empty', () => {
      // Arrange
      (snackService as any).isInstanceVisible = false;
      (snackService as any).msgQueue = [];

      // Act
      snackService.openSnackBar();

      // Assert
      expect(matSnackBar.openFromComponent).not.toHaveBeenCalled();
    });

    test('should open when instance is not visible', () => {
      // Arrange
      const spy = jest.spyOn(snackService, 'showNext');
      (snackService as any).isInstanceVisible = false;

      // Act
      snackService.openSnackBar();

      // Assert
      expect(spy).toHaveBeenCalled();
    });

    test('should not open when instance is visible', () => {
      // Arrange
      const spy = jest.spyOn(snackService, 'showNext');
      (snackService as any).isInstanceVisible = true;

      // Act
      snackService.openSnackBar();

      // Assert
      expect(spy).not.toHaveBeenCalled();
    });
  });

  describe('msgQueue', () => {
    test('should contain 1 element when openSnackBar has been called 1 time with config', () => {
      // Arrange
      const expected = 1;
      (snackService as any).isInstanceVisible = true;

      // Act
      snackService.openSnackBar({} as MatSnackBarConfig);
      const actual = (snackService as any).msgQueue.length;

      // Assert
      expect(expected).toEqual(actual);
    });

    test('should contain 2 elements if openSnackBar has been called 2 times with config', () => {
      // Arrange
      const expected = 2;
      (snackService as any).isInstanceVisible = true;

      // Act
      snackService.openSnackBar({} as MatSnackBarConfig);
      snackService.openSnackBar({} as MatSnackBarConfig);
      const actual = (snackService as any).msgQueue.length;

      // Assert
      expect(expected).toEqual(actual);
    });
  });

  describe('ngOnDestroy', () => {
    test('should trigger destroy$ when invoked', () => {
      // Arrange
      const spy = jest.spyOn((snackService as any).destroy$, 'next');

      // Act
      snackService.ngOnDestroy();

      // Assert
      expect(spy).toHaveBeenCalled();
    });
  });
});
