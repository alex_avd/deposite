import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgxsFormPluginModule } from '@ngxs/form-plugin';
import { MaterialModule } from '../material';
import { SnackBarComponent } from './components/snack-bar/snack-bar.component';
import { HasPermissions } from './directives/has-permissions-directive';
import { DefaultModalComponent } from './components/default-modal/default-modal.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    NgxsFormPluginModule,
  ],
  declarations: [
    SnackBarComponent,
    HasPermissions,
    DefaultModalComponent,
  ],
  exports: [HasPermissions],
  entryComponents: [],
})
export class SharedModule {

}
