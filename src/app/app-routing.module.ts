import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './core/components/login/login.component';
import { LayoutComponent } from './core/layouts/layout/layout.component';
import { LoginLayoutComponent } from './core/layouts/login-layout/login-layout.component';
import { RoleGuardService } from './shared/guards/role-guard-service';
import { AuthGuardService } from './shared/guards/auth-guard-service';

const routes: Routes = [

  { path: '', pathMatch: 'full', redirectTo: 'dashboard' },
  {
    path: 'dashboard',
    canActivate: [AuthGuardService],
    component: LayoutComponent,
    children: [{
      path: '',
      loadChildren: () => import('./dashboard/dashboard.module')
        .then((m) => m.DashboardModule),
    }],
  },
  {
    path: 'loadings',
    canActivate: [AuthGuardService],
    component: LayoutComponent,
    children: [{
      path: '',
      loadChildren: () => import('./loadings/loadings.module')
        .then((m) => m.LoadingsModule),
    }],
  },
  {
    path: 'login',
    component: LoginLayoutComponent,
    children: [{
      path: '',
      component: LoginComponent,
    }],
  },
  { path: '**', redirectTo: 'dashboard' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes,
      {
        useHash: true,
        // preloadingStrategy: PreloadAllModules,
      })],
  exports: [RouterModule],
  providers: [
    RoleGuardService,
    AuthGuardService,
  ],
})
export class AppRoutingModule {
}
