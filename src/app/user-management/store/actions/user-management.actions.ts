/* eslint-disable padded-blocks */

import { UserModel } from '../../models/user.model';

export class GetUserManagementList {
  static readonly type = '[User Manager] Get User Management List';

  constructor() {
  }
}

export class GetUserManagementListSuccess {
  static readonly type = '[User Manager] Get User Management List Success';

  constructor(public userList: UserModel[]) {
  }
}

export class GetUserManagementData {
  static readonly type = '[User Manager] Get User Management Data';

  constructor(public userId: number) {
  }
}

export class GetUserManagementDataSuccess {
  static readonly type = '[User Manager] Get User Management Data Success';

  constructor(public userData: UserModel) {
  }
}

export class UpdateUserManagementData {
  static readonly type = '[User Manager] Update User Management Data';

  constructor(public userId: number) {
  }
}

export class UpdateUserManagementDataSuccess {
  static readonly type = '[User Manager] Update User Management Data Success';

  constructor(public userData: UserModel) {
  }
}

export class DeleteUser {
  static readonly type = '[User Manager] DeleteUser';

  constructor(public userId: number) {
  }
}


export class DeleteUserSuccess {
  static readonly type = '[User Manager] DeleteUser Success';
}



