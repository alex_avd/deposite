import { Injectable } from '@angular/core';
import {
  Action, Selector, State,
  StateContext,
} from '@ngxs/store';
import { map, tap } from 'rxjs/operators';
import { SelectSnapshot } from '@ngxs-labs/select-snapshot';
import { Params } from '@angular/router';
import { UpdateFormValue } from '@ngxs/form-plugin';
import { UserModel } from '../models/user.model';
import { UserManagementDataService } from '../services/user-management-data.service';
import { SnackbarService } from '../../shared/services/snack-bar.service';
import { RouterSelectors } from '../../core/store/router.selectors';
import {
  DeleteUser, DeleteUserSuccess,
  GetUserManagementData,
  GetUserManagementDataSuccess,
  GetUserManagementList,
  GetUserManagementListSuccess, UpdateUserManagementData, UpdateUserManagementDataSuccess,
} from './actions/user-management.actions';

interface UserManagementStateModel {
  userList: UserModel[];
  userData: UserModel;
  userDetailsForm: {
    model: any;
    dirty: boolean;
    status: string;
    errors: any;
  }
}

const defaultState: UserManagementStateModel = {
  userList: null,
  userData: null,
  userDetailsForm: {
    model: undefined,
    dirty: false,
    status: '',
    errors: {},
  },
};

@State<UserManagementStateModel>({
  name: 'userManagement',
  defaults: defaultState,
})

@Injectable()
export class UserManagementState {

  constructor(
    private userManagementDataService: UserManagementDataService,
    private snackbarService: SnackbarService,
  ) {
  }

  @SelectSnapshot(RouterSelectors.params) params: Params;

  @Selector()
  static userManagementList(state: UserManagementStateModel) {
    return state.userList;
  }

  @Selector()
  static userData(state: UserManagementStateModel) {
    return state.userData;
  }


  @Action(GetUserManagementList)
  getUserManagementList(ctx: StateContext<UserManagementStateModel>) {
    return this.userManagementDataService.getUserManagementListData().pipe(
      tap((result) => {
        ctx.dispatch(new GetUserManagementListSuccess(result.data));
      }),
    );
  }

  @Action(GetUserManagementListSuccess)
  getUserManagementListSuccess(ctx: StateContext<UserManagementStateModel>, action: GetUserManagementListSuccess) {
    ctx.patchState({
      userList: action.userList,
    });

  }

  @Action(GetUserManagementData)
  getUserManagementData(ctx: StateContext<UserManagementStateModel>, action: GetUserManagementData) {
    return this.userManagementDataService.getUserManagementById(action.userId).pipe(
      tap((result) => {
        ctx.dispatch(new GetUserManagementDataSuccess(result.data));
      }),
    );
  }

  @Action(GetUserManagementDataSuccess)
  getUserManagementDataSuccess(ctx: StateContext<UserManagementStateModel>, action: GetUserManagementDataSuccess) {
    ctx.patchState({
      userData: action.userData,
    });

    console.log(action.userData)
    const userForm = {
      ...action.userData,
      userRoleId: action.userData.userRole.id,
      availablePlantsIds: action.userData.availablePlants.map(plant => plant.id)
    }

    console.log(userForm)

    ctx.dispatch(new UpdateFormValue({
      value: userForm,
      path: 'userManagement.userDetailsForm',
    }));


  }

  @Action(UpdateUserManagementData)
  updateUserManagementData(ctx: StateContext<UserManagementStateModel>, action: UpdateUserManagementData) {
    const userData = ctx.getState().userDetailsForm.model;

    return this.userManagementDataService.updateUserManagementById(action.userId, userData).pipe(
      map((result) => {
        ctx.dispatch(new UpdateUserManagementDataSuccess(result.data));
      })
    );
  }

  @Action(UpdateUserManagementDataSuccess)
  updateUserManagementDataSuccess(ctx: StateContext<UserManagementStateModel>, action: UpdateUserManagementDataSuccess) {
    this.openSuccessSnackbar('User updated successfully');

    ctx.dispatch(new GetUserManagementData(action.userData.id));
  }

  @Action(DeleteUser)
  deleteUser(ctx: StateContext<UserManagementStateModel>, action: DeleteUser) {

    return this.userManagementDataService.deleteUser(action.userId).pipe(
      map(() => {
        ctx.dispatch(new DeleteUserSuccess());
      })
    );
  }

  @Action(DeleteUserSuccess)
  deleteUserSuccess(ctx: StateContext<UserManagementStateModel>) {
    this.openSuccessSnackbar('User deleted successfully');
    ctx.dispatch(new GetUserManagementList());
  }

  private openSuccessSnackbar(message: string) {
    this.snackbarService.openSnackBar({
      data: { message },
      horizontalPosition: 'start',
      duration: 4000,
    });
  }

}
