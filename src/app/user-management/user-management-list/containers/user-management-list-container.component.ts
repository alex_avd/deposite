import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { UserManagementStoreService } from '../../services/user-management-store.service';
import { ModalService } from '../../../shared/services/modal.service';
import { UserManagementDetailsComponent } from '../components/user-management-details/user-management-details.component';
import { UserRoleModel } from '../../../core/models/user-role.model';
import { PlantModel } from '../../../core/models/plant.model';

@Component({
  selector: 'app-user-management-list-container',
  templateUrl: './user-management-list-container.component.html',
  styleUrls: ['./user-management-list-container.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class UserManagementListContainerComponent implements OnInit {

  userManagementList$ = this.userManagementStoreService.userManagementList$;

  displayedColumns: string[] = [
    'id',
    'firstName',
    'role',
    'email',
    'streetAddress',
    'secondStreetAddress',
    'country',
    'city',
    'state',
    'zipCode',
    'delete',
  ];

  constructor(
    private userManagementStoreService: UserManagementStoreService,
    private modalService: ModalService,
  ) {
  }

  ngOnInit(): void {
    this.userManagementStoreService.getUserManagementList();
  }

  openUserDetailsDialog(userId) {
    this.userManagementStoreService.getUserManagementData(userId);
    this.modalService.openModal({
      modalComponent: UserManagementDetailsComponent,
    }, {
      width: 'auto',
      data: {
        dialogRef: () => this.modalService.getDialogRef(UserManagementDetailsComponent),
        userId: userId,
      },
      autoFocus: false,
      disableClose: true,
    });
  }

  deleteUser(userId) {
    this.userManagementStoreService.deleteUser(userId);
  }

}
