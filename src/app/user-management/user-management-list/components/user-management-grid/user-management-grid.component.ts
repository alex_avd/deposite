import {
  Component,
  EventEmitter, Input, Output, ViewEncapsulation,
} from '@angular/core';
import { Observable } from 'rxjs';
import { UserModel } from '../../../models/user.model';

@Component({
  selector: 'app-user-management-grid',
  templateUrl: './user-management-grid.component.html',
  styleUrls: ['./user-management-grid.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class UserManagementGridComponent {

  @Input() public displayedColumns: string[];
  @Input() public userManagementList$: Observable<UserModel[]>;

  @Output() private openUserDetailsDialog: EventEmitter<number> = new EventEmitter();
  @Output() private deleteUser: EventEmitter<number> = new EventEmitter();

  onOpenUserDetails(event) {
    this.openUserDetailsDialog.emit(event)
  }

  onDelete(userId) {
    this.deleteUser.emit(userId);
  }


}
