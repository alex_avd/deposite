import {
  Component,
  EventEmitter, Inject, Input, OnDestroy, OnInit, Output, ViewEncapsulation,
} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { UserModel } from '../../../models/user.model';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserManagementDetailsModalDataModel } from '../../../models/user-management-details-modal-data.model';
import { ModalConfig } from '../../../../shared/models/modal-config';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { UserManagementStoreService } from '../../../services/user-management-store.service';
import { AVAILABLE_PLANTS, USER_ROLES } from '../../../../shared/constants/form-constants';
import { Actions, ofActionSuccessful } from '@ngxs/store';
import { takeUntil, tap } from 'rxjs/operators';
import { UpdateUserManagementDataSuccess } from '../../../store/actions/user-management.actions';

@Component({
  selector: 'app-user-management-details',
  templateUrl: './user-management-details.component.html',
  styleUrls: ['./user-management-details.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class UserManagementDetailsComponent implements OnInit {

  @Input() public displayedColumns: string[];
  @Input() public userManagementList$: Observable<UserModel[]>;

  @Output() private openUserDetailsDialog: EventEmitter<number> = new EventEmitter();

  userRoles = USER_ROLES;
  availablePlants = AVAILABLE_PLANTS;
  userId: number;
  config: ModalConfig = {
    okButton: 'OK',
    bodyText: 'Body text',
    cancelButton: 'CANCEL',
    headerText: 'Edit user',
  };
  userDetailsForm: FormGroup = this.fb.group({
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
    email: ['', Validators.required],
    streetAddress: ['', Validators.required],
    secondStreetAddress: ['', Validators.required],
    city: ['', Validators.required],
    state: ['', Validators.required],
    country: ['', Validators.required],
    zipCode: ['', Validators.required],
    userRoleId: ['', Validators.required],
    availablePlantsIds: ['', Validators.required],
  });


  constructor(
    private fb: FormBuilder,
    private actions$: Actions,
    private storeService: UserManagementStoreService,
    @Inject(MAT_DIALOG_DATA) public componentInput: UserManagementDetailsModalDataModel,
  ) {
    if (this.componentInput.userId) {
      this.userId = this.componentInput.userId;
    }
  }

  ngOnInit(): void {
  }

  onSubmit() {
    if (this.userDetailsForm.valid) {
      this.storeService.updateUser(this.userId);
      this.componentInput.dialogRef().close();
    }
  }

}
