import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexModule } from '@angular/flex-layout';
import { NgxsModule } from '@ngxs/store';

import { MaterialModule } from '../../material';
import { SharedModule } from '../../shared/shared.module';
import { UserManagementListContainerComponent } from './containers/user-management-list-container.component';
import { UserManagementGridComponent } from './components/user-management-grid/user-management-grid.component';
import { UserManagementListRoutingModule } from './user-management-list.routing.module';
import { UserManagementState } from '../store/user-management.state';
import { UserManagementDetailsComponent } from './components/user-management-details/user-management-details.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxsFormPluginModule } from '@ngxs/form-plugin';

@NgModule({
  declarations: [
    UserManagementListContainerComponent,
    UserManagementGridComponent,
    UserManagementDetailsComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    UserManagementListRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxsModule.forFeature([UserManagementState]),
    NgxsFormPluginModule,
    FlexModule,
  ],
  providers: [],
  entryComponents: [
    // UserManagementDetailsComponent
  ]
})
export class UserManagementListModule {
}
