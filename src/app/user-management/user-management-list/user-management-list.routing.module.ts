import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { UserManagementListContainerComponent } from './containers/user-management-list-container.component';

const routes: Routes = [
  { path: '', component: UserManagementListContainerComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class UserManagementListRoutingModule {

}
