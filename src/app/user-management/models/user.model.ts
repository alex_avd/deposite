import { UserRoleModel } from '../../core/models/user-role.model';
import { PlantModel } from '../../core/models/plant.model';

export interface UserModel {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  streetAddress?: string;
  secondStreetAddress?: string;
  city?: string;
  state?: string;
  country?: string;
  zipCode?: string;
  userRole?: UserRoleModel;
  availablePlants?: PlantModel[];
}
