import { Injectable } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Dispatch } from '@ngxs-labs/dispatch-decorator';
import {
  DeleteUser,
  GetUserManagementData,
  GetUserManagementList,
  UpdateUserManagementData,
} from '../store/actions/user-management.actions';
import { UserManagementState } from '../store/user-management.state';
import { UserModel } from '../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class UserManagementStoreService {

  @Select(UserManagementState.userManagementList) userManagementList$: Observable<UserModel[]>;
  @Select(UserManagementState.userData) userData$: Observable<UserModel>;

  @Dispatch()
  getUserManagementList = () => new GetUserManagementList();

  @Dispatch()
  getUserManagementData = (userId: number) => new GetUserManagementData(userId);

  @Dispatch()
  updateUser = (userId: number) => new UpdateUserManagementData(userId);

  @Dispatch()
  deleteUser = (userId: number) => new DeleteUser(userId);

}

