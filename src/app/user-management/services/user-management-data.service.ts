import { Injectable } from '@angular/core';

import { UserModel } from '../models/user.model';
import { AppConfig } from '../../core/infrastructure/services/app-config.service';
import { HttpClientService } from '../../shared/services/http-client.service';

@Injectable({
  providedIn: 'root',
})
export class UserManagementDataService {

  private get url(): string {
    return AppConfig.settings.API.AUTOMATION_STANDARD;
  }

  constructor(private httpClientService: HttpClientService) {
  }

  getUserManagementListData() {
    return this.httpClientService.get<UserModel[]>(`${this.url}/users-management/users`);
  }

  getUserManagementById(userId: number) {
    return this.httpClientService.get<UserModel>(`${this.url}/users-management/users/${userId}`);
  }

  updateUserManagementById(userId: number, userData: UserModel) {
    return this.httpClientService.put<UserModel>(`${this.url}/users-management/users/${ userId }`, userData);
  }

  deleteUser(userId: number) {
    return this.httpClientService.delete<number>(`${this.url}/users-management/users/${ userId }`);
  }


}
