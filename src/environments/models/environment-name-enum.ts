export enum EnvironmentName {
  FAKE = 'fake' as any,
  DEV = 'dev' as any,
  PROD = 'prod' as any,
  LOCAL = 'local' as any,
  STAGE = 'stage' as any,
}
