import { EnvironmentName } from './environment-name-enum';

export interface Environment {
  production: boolean;
  envName: EnvironmentName;
  appNamespace?: string;
}
