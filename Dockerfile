FROM nginx:1.19.7

LABEL app=appdev

COPY dist/** /usr/share/nginx/html

COPY nginx_configs/app.conf /etc/nginx/conf.d/app.conf
COPY nginx_configs/nginx.conf /etc/nginx/nginx.conf

EXPOSE 80 
CMD ["nginx", "-g", "daemon off;"]
